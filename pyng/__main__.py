import pygame
import sys
from pygame.constants import K_RIGHT, K_SPACE, K_a
import pygame.key as key
from pygame import Rect
from objects import Pong, Ball, FuelBar
from __init__ import (
    screen,
    width,
    height,
    FPS,
    seperator_width,
    score_font,
    win_font,
    speed_up_factor,
)
from event import PLAYER_SCORED

left_player_score = 0
right_player_score = 0


def draw_background():
    global left_player_score, right_player_score
    pygame.draw.line(
        screen,
        (255, 255, 255),
        (width // 2, 0),
        (width // 2, height),
        seperator_width,
    )

    score_surface = score_font.render(str(left_player_score), False, (255, 255, 255))
    screen.blit(score_surface, (width / 4 - score_surface.get_width() / 2, 50))
    score_surface = score_font.render(str(right_player_score), False, (255, 255, 255))
    screen.blit(score_surface, (width * 3 / 4 - score_surface.get_width() / 2, 50))

    left_fuel_bar.draw(screen, left_fuel_bar.get_rect())
    right_fuel_bar.draw(screen, right_fuel_bar.get_rect())


left_pong = Pong(50, height // 2)
left_pong.move_position(-left_pong.get_width() / 2, -left_pong.get_height() / 2)
right_pong = Pong(width - 50, height // 2)
right_pong.move_position(-right_pong.get_width() / 2, -right_pong.get_height() / 2)
ball = Ball(width // 2, height // 2)
ball.reset()
left_fuel_bar = FuelBar(Rect(5, 5, 300, 30))
right_fuel_bar = FuelBar(Rect(width - 5 - 300, 5, 300, 30))


def move_objects(delta_t):
    # move objects
    for obj in [left_pong, right_pong, ball]:
        obj.update_position(delta_t)


def handle_collisions():
    ball_rect = ball.get_rect()
    ball_speed = ball.get_speed()
    # check collisions and react
    for pong, ball_is_comming in zip(
        [left_pong, right_pong], [ball_speed.x < 0, ball_speed.x > 0]
    ):
        if ball_rect.colliderect(pong.get_rect()) and ball_is_comming:
            ball_speed.x = -ball_speed.x
            ball.set_speed(ball_speed)
            ball.chop_y(pong.get_speed().y)
            ball.update_speed(speed_up_factor)


def win():
    if left_player_score >= 10:
        win_surface = win_font.render(str("LEFT PLAYER WON"), False, (255, 0, 0))
        screen.blit(
            win_surface,
            (
                width // 2 - (win_surface.get_width() // 2),
                height // 2 - (win_surface.get_height() // 2),
            ),
        )
    elif right_player_score >= 10:
        win_surface = win_font.render(str("RIGHT PLAYER WON"), False, (0, 0, 255))
        screen.blit(
            win_surface,
            (
                width // 2 - (win_surface.get_width() // 2),
                height // 2 - (win_surface.get_height() // 2),
            ),
        )


def update_screen():
    screen.fill((0, 0, 0))
    draw_background()
    left_pong.draw(screen, left_pong.get_rect())
    right_pong.draw(screen, right_pong.get_rect())
    ball.draw(screen, ball.get_rect())
    win()
    pygame.display.update()


def handle_keypresses(delta_t: int):
    key_pressed = key.get_pressed()
    if key_pressed[pygame.K_w] ^ key_pressed[pygame.K_s]:
        if key_pressed[pygame.K_w]:
            left_pong.set_speed(0.0, -500)
        else:
            left_pong.set_speed(0.0, 500)
    else:
        left_pong.set_speed(0.0, 0.0)

    if key_pressed[pygame.K_UP] ^ key_pressed[pygame.K_DOWN]:
        if key_pressed[pygame.K_UP]:
            right_pong.set_speed(0.0, -500.0)
        else:
            right_pong.set_speed(0.0, 500.0)
    else:
        right_pong.set_speed(0.0, 0.0)

    if key_pressed[K_a] or key_pressed[K_SPACE]:
        left_pong.use_fuel(delta_t)
    else:
        left_pong.fill_fuel(delta_t)
    left_fuel_bar.set_value(left_pong.get_fuel())
    if key_pressed[K_RIGHT]:
        right_pong.use_fuel(delta_t)
    else:
        right_pong.fill_fuel(delta_t)
    right_fuel_bar.set_value(right_pong.get_fuel())


def handle_events():
    global left_player_score, right_player_score
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit(0)
        elif event.type == PLAYER_SCORED:
            if event.side == "left":
                left_player_score += 1
            else:
                right_player_score += 1


def main():
    clock = pygame.time.Clock()
    while True:
        delta_t = clock.get_time()
        handle_events()
        handle_keypresses(delta_t)
        move_objects(delta_t)
        handle_collisions()
        update_screen()
        clock.tick(FPS)


if __name__ == "__main__":
    main()
