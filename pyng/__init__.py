import pygame

pygame.init()
pygame.font.init()
pygame.display.set_caption("pyng")

width = 1300
height = width * 3 // 4
seperator_width = 20
screen = pygame.display.set_mode((width, height))
FPS = 60
speed_up_factor = 1.1

score_font = pygame.font.SysFont(pygame.font.get_default_font(), 150)
win_font = pygame.font.SysFont(pygame.font.get_default_font(), 170)
