from pygame.event import Event, custom_type

PLAYER_SCORED = custom_type()


def PlayerScoredEvent(side: str):
    return Event(PLAYER_SCORED, {"side": side})
