from typing import overload
import random
from pygame import Rect, Color, Surface, Vector2, draw
from pygame.event import post as post_event
from math import sin, cos, atan2, radians, degrees, sqrt
from abc import ABC
from __init__ import width, height
from event import PlayerScoredEvent


class Drawable(ABC):
    def __init__(self, x: int, y: int, width: int, height: int):
        self._surface: Surface = Surface((width, height))
        self._rect = Rect(x, y, width, height)

    def draw(self, screen: Surface, rect: Rect):
        screen.blit(self._surface, self._rect)

    def get_rect(self):
        return Rect(self._rect)

    def get_height(self):
        return self._rect.height

    def get_width(self):
        return self._rect.width

    def get_x(self):
        return self._rect.x

    def get_y(self):
        return self._rect.y


class Moveable(Drawable):
    def __init__(self, x: float, y: float, width: int, height: int):
        super().__init__(x, y, width, height)
        self._position = Vector2(x, y)
        self._speed = Vector2(0, 0)

    def get_position(self):
        return Vector2(self._position)

    def set_position(self, x: float, y: float):
        self._position = Vector2(x, y)
        self._rect.x = int(x)
        self._rect.y = int(y)

    @overload
    def move_position(self, x: float, y: float):
        ...

    @overload
    def move_position(self, delta: Vector2):
        ...

    def move_position(self, *args):
        if len(args) == 1:
            self._position += args[0]
        else:
            self._position += Vector2(args[0], args[1])
        self._rect.x = int(self._position.x)
        self._rect.y = int(self._position.y)

    def set_speed_polar(
        self,
        amount,
        alpha,
    ):
        self._speed = Vector2(
            amount * cos(radians(alpha)), amount * sin(radians(alpha))
        )

    def get_speed_polar(self):
        amount = sqrt(self._speed.x * self._speed.x + self._speed.y * self._speed.y)
        if self._speed.x == 0:
            if self._speed.y > 0:
                alpha = 90
            else:
                alpha = 270
        elif self._speed.y > 0:
            alpha = degrees(atan2(self._speed.y, self._speed.x))
        else:
            alpha = degrees(atan2(self._speed.y, self._speed.x)) + 360
        return amount, alpha

    @overload
    def set_speed(self, speed: Vector2):
        ...

    @overload
    def set_speed(self, x: int, y: int):
        ...

    def set_speed(self, *args):
        if len(args) == 1:
            self._speed = args[0]
        else:
            self._speed = Vector2(args[0], args[1])

    def get_speed(self):
        return Vector2(self._speed)

    def get_x(self):
        return self._position.x

    def get_y(self):
        return self._position.y

    def update_position(self, delta_t: float):
        delta_pos = self.get_speed() * (delta_t / 1000.0)
        self.move_position(delta_pos)


class Ball(Moveable):
    base_speed = 500.0
    chop_factor = 0.0012

    def __init__(self, x, y):
        super().__init__(x, y, 25, 25)
        self._surface.fill(Color(255, 0, 255))

    def reset(self):
        random_alpha = random.randrange(-45, 60)
        self.set_speed_polar(Ball.base_speed, random_alpha)
        if random.randrange(2) == 1:
            self._speed.x = -self._speed.x
        self.set_position(
            width // 2 - (self._rect.width // 2),
            height // 2 - (self._rect.height // 2),
        )

    def chop_y(self, other_speed_y):
        speed = self.get_speed()
        if other_speed_y == self.get_speed().y:
            return
        amount, alpha = self.get_speed_polar()
        speed.y += Ball.chop_factor * other_speed_y * abs(speed.y - other_speed_y)
        self.set_speed(speed)
        _, alpha = self.get_speed_polar()
        if alpha > 60 and alpha <= 90:
            alpha = 60
        elif alpha > 90 and alpha < 120:
            alpha = 120
        elif alpha > 250 and alpha <= 270:
            alpha = 250
        elif alpha > 270 and alpha < 300:
            alpha = 300
        self.set_speed_polar(amount, alpha)

    def update_speed(self, factor):
        amount, alpha = self.get_speed_polar()
        amount *= factor
        self.set_speed_polar(amount, alpha)
        return amount

    def update_position(self, delta_t: float):
        super().update_position(delta_t)
        if (self._rect.y <= 0 and self._speed.y < 0) or (
            self._rect.y >= height - self._rect.height and self._speed.y > 0
        ):
            self._speed.y = -self._speed.y
        if self._rect.x < 0 or self._rect.x > width:
            post_event(PlayerScoredEvent("right" if self._rect.x < 0 else "left"))
            self.reset()


class Pong(Moveable):
    def __init__(self, x, y):
        super().__init__(x, y, 30, 200)
        self._speed_boost = False
        self._surface.fill(Color(255, 255, 255))
        self._fuel = 100.0

    def get_speed(self):
        if self._speed_boost:
            return Vector2(self._speed.x, self._speed.y * 2.0)
        return Moveable.get_speed(self)

    def get_fuel(self):
        return self._fuel

    def use_fuel(self, delta_t):
        self._fuel -= delta_t * 100.0 / 1000.0
        if self._fuel < 0:
            self._fuel = 0
        if self._fuel > 0:
            self._speed_boost = True
        else:
            self._speed_boost = False

    def fill_fuel(self, delta_t):
        self._fuel += delta_t * 50.0 / 1000.0
        if self._fuel > 100.0:
            self._fuel = 100.0
        self._speed_boost = False

    def update_position(self, delta_t: float):
        super().update_position(delta_t)
        if self.get_y() < 0:
            self.set_position(self.get_x(), 0.0)
        elif self.get_y() > height - self.get_height():
            self.set_position(self.get_x(), height - self.get_height())


class FuelBar(Drawable):
    def __init__(self, rect: Rect):
        super().__init__(rect.x, rect.y, rect.width, rect.height)
        self._value = 0.0
        self.set_value(100.0)
        draw.rect(self._surface, Color(255, 255, 255), self._surface.get_rect(), 2)

    def set_value(self, value):
        if value == self._value:
            return
        self._value = value
        inner_rect = self._surface.get_rect()
        inner_rect.inflate_ip(-10, -10)
        inner_rect.move(10, 10)
        draw.rect(self._surface, Color(0, 0, 0), inner_rect)
        inner_rect.width = int(inner_rect.width * self._value / 100.0)
        draw.rect(self._surface, Color(255, 255, 255), inner_rect)
